//Inspiré et adapté à partir du projet SDP
export default {
    tokenIsInvalid() {
      const tokenInDb = JSON.parse(localStorage.getItem('token'));
  
      if (tokenInDb) {
        const now = new Date();
        const dateInDB = new Date(parseInt(tokenInDb.expire_in));
        if (parseInt(dateInDB.getHours()) >= parseInt(now.getHours())) {
          return !true;
        }
        return !false;
      }
      return !false;
    },
    deconnectUser() {
      localStorage.removeItem('token');
      localStorage.removeItem('user');
    },
  };
  