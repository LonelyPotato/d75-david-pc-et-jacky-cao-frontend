//Fait par Jacky//

export default {
    authHeader() {
        // return authorization header with jwt token
        let user = JSON.parse(localStorage.getItem('user'));

        if (user && user.token) {
            return {
                'Authorization': 'Bearer ' + user.token
            };
        } else {
            return {};
        }
    },

    getEmail() {
        return JSON.parse(localStorage.getItem('user')).Email;
    },

    deconnectUser() {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
    },
};