//Fait par Jacky et David//

import axios from 'axios';

export default {
	namespaced: true,

	state() {
		return {
			all: [],
			livre: "",
			livreId: 0,
			livreTitle: "",
		};
	},

	actions: {
		getAllLivres({ commit }) {
			return axios.get(`api/Livres`).then((response) => {
				commit('UPDATE_COLLECTION', { items: response.data });
			});
		},

		getLivreParId({ commit }, id) {
			return axios.get(`api/Livre/${id}`).then((response) => {
				commit('UPDATE_LIVRE', response.data);
			});
		},
		
		getLivreTitleParId({ commit }, id) {
			return axios.get(`api/Livre/${id}`).then((response) => {
				commit('UPDATE_LIVRE_TITRE', response.data);
			})
		},

		getLivreParIdMatiere({ commit }, id) {
			return axios.get(`api/LivresParIdMatiere/${id}`).then((response) => {
				commit('UPDATE_COLLECTION', { items: response.data });
			});
		},

		setLivreId({ commit }, newId) {
			localStorage.setItem('livreId', newId);
			commit('UPDATE_LIVRE_ID_CHANGE', newId);
		},
	},

	mutations: {
		UPDATE_COLLECTION(state, { items }) {
			state.all = []; //Pour ne pas que les données se dédoublent
			items.forEach((livre) => {
				state.all.push(livre);
			});
		},
		
		UPDATE_LIVRE_TITRE(state, newTitle) {
			state.livreTitle = newTitle.Titre;
		},

		UPDATE_LIVRE_ID_CHANGE(state, newId) {
			state.livreId = newId;
		},

		UPDATE_LIVRE(state, livre) {
			state.livre = livre;
		}
	}

}