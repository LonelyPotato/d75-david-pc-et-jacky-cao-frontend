//Fait par Jacky et David//

import Vue from 'vue'
import Vuex from 'vuex'

import livres from './livres/index';
import matieres from './matieres/index';
import authentication from './authentication/index';
import itemlivres from './itemlivres/index';
import commandes from './commandes/index';

Vue.use(Vuex)

export default new Vuex.Store({
  state() {
    return {};
  },
  
  mutations: {},
  actions: {},

  modules: {
    livres,
    matieres,
    authentication,
    itemlivres,
    commandes
  },
})
