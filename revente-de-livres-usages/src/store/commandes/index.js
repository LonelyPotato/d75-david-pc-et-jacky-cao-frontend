//Fait par Jacky//

import axios from 'axios';

export default {
	namespaced: true,

	state() {
		return {
			all: [],
			oneCommande: { },
			allItem: [],
		};
	},

	actions: {
		getCommandesByUser({ commit }, username) {
			return axios.get(`api/CommandeByClient/${username}`)
			.then((response) => {
				commit('UPDATE_COLLECTION', { items: response.data });
			});
		},

		getCommandeByNoCommande({ commit }, noCommande) {
			return axios.get(`api/CommandeParNoCommande/${noCommande}`)
			.then((response) => {
				commit('UPDATE_ONE_LIVRE', response.data );
			});
		},

		getIdItemLivresByCommande({ commit }, username) {
			return axios.get(`api/CommandeByClient/${username}`)
			.then((response) => {
				commit('UPDATE_COLLECTION_ITEM_LIVRE', { items: response.data.IdItemAchete });
			});
		},

		setNumeroCommande({ commit }, no) {
			localStorage.setItem('numeroCommande', no);
		},
	},

	mutations: {
		UPDATE_COLLECTION(state, { items }) {
			state.all = [];
			items.forEach((commande) => {
				state.all.push(commande);
			});
		},

		UPDATE_COLLECTION_ITEM_LIVRE(state, { items }) {
			state.allItem = [];
			items.forEach((itemlivre) => {
				if(itemlivre.Statut == 0) {
					state.allItem.push(itemlivre);
				}
			});
		},

		UPDATE_ONE_LIVRE(state, item) {
			state.oneCommande = item;
		},
	}

}