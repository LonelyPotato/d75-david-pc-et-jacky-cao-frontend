//Fait par Jacky//

import axios from 'axios';

export default {
  namespaced: true,

  state() {
    return {
      vendeur: 0,
      vendeurDetail: '',
      currentUser: '',
      currentUsername: '',
    };
  },
  actions: {
    getUserByUsername({ commit }, username) {
      return axios.get(`/api/UserByUsername/${username}/`)
        .then((response) => {
          const user = JSON.stringify(response.data);
          localStorage.setItem('user', user);
          localStorage.setItem('userUsername', user.Username)
          commit('APPLY_USER', user);
        });
    },

    getVendeurByUsername({ commit }, username) {
      return axios.get(`/api/UserByUsername/${username}/`)
        .then((response) => {
          const vendeur = response.data;
          commit('APPLY_VENDEUR', vendeur.Id);
        })
        .catch((error) => {
          commit('APPLY_VENDEUR', 0);
        });
    },

    getUserById({ commit }, id) {
      return axios.get(`/api/User/${id}`)
        .then((response) => {
          commit('APPLY_VENDEUR_DETAIL', response.data);
        });
    },

    readCurrentUser({ commit }) {
      const value = JSON.parse(localStorage.getItem('user'));
      commit('APPLY_USER', value);
    },

    readCurrentUsername({ commit }) {
      const value = localStorage.getItem('userUsername');
      commit('APPLY_USERNAME', value);
    },

    removeCurrentUser({ commit }) {
      localStorage.removeItem('user');
      commit('APPLY_USER', null);
    },
  },
  mutations: {
    APPLY_VENDEUR(state, vendeur) {
      state.vendeur = vendeur;
    },
    APPLY_USER(state, newUser) {
      state.currentUser = newUser;
    },
    APPLY_USERNAME(state, newUsername) {
      state.currentUsername = newUsername;
    },
    APPLY_VENDEUR_DETAIL(state, vendeur) {
      state.vendeurDetail = vendeur;
    },
  },
};
