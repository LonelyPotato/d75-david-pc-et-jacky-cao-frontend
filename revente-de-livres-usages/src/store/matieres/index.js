//Fait par David//

import axios from 'axios';

export default {
	namespaced: true,

	state() {
		return {
			all: [],
		};
	},

	actions: {
		getAllMatieres({ commit }) {
			return axios.get(`api/Matieres`).then((response) => {
				commit('UPDATE_COLLECTION', { items: response.data });
			});
		},
	},

	mutations: {
		UPDATE_COLLECTION(state, { items }) {
      state.all = []; //Pour ne pas que les données se dédoublent
			items.forEach((livre) => {
				state.all.push(livre);
			});
		}
	}

}