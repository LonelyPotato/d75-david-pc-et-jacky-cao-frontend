//Fait par David et Jacky//

import axios from 'axios';

export default {
	namespaced: true,

	state() {
		return {
			all: [],
			allItem: [],
			allVentes: [],
			itemLivre: { },
			filtrePrix: 1000,
			filtreEtat: 3,
		};
	},

	actions: {
		
		getItemLivresByVendeur({ commit }, id) {
			return axios.get(`api/ItemLivreByVendeurId/${id}`)
			.then((response) => {
				commit('UPDATE_COLLECTION_VENDEUR', { items: response.data });
			});
		},
		
		getItemLivresParIdLivre({ commit }, id) {
			return axios.get(`api/ItemLivresParIdLivre/${id}`).then((response) => {
				commit('UPDATE_COLLECTION', { items: response.data });
			});
		},

		getItemLivreParId({ commit }, id) {
			return axios.get(`api/ItemLivre/${id}`).then((response) => {
				commit('UPDATE_ITEMLIVRE', response.data);
			});
		},

		setItemLivreIdLocalStorage({ commit }, id) {
			localStorage.setItem('itemLivreId', id);
		},

		updateFiltrePrix({ commit }, prix) {
			commit('UPDATE_FILTRE_PRIX', prix);
		},

		updateFiltreEtat({ commit }, etat) {
			commit('UPDATE_FILTRE_ETAT', etat);
		},
	},

	mutations: {
		UPDATE_COLLECTION(state, { items }) {
			state.all = []; 
			items.forEach((livre) => {
				if(livre.Statut == 0)
					state.all.push(livre);
			});
		},

		UPDATE_COLLECTION_VENDEUR(state, { items }) {
			state.allVentes = [];
			items.forEach((commande) => {
				state.allVentes.push(commande);
			});
		},

		UPDATE_ITEMLIVRE(state, item) {
			state.itemLivre = item;
		},

		UPDATE_FILTRE_PRIX(state, prix) {
			state.filtrePrix = prix;
		},

		UPDATE_FILTRE_ETAT(state, etat) {
			state.filtreEtat = etat;
		},
	},

}