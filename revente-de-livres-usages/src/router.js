import Vue from 'vue';
import Router from 'vue-router';
import Recherche from '@/components/Recherche.vue';
import Login from '@/components/Login/Login.vue';
import SignUp from '@/components/Login/SignUp.vue';
import AdminPage from '@/components/Admin/AdminPage.vue';
import UserPage from '@/components/User/UserPage.vue';
import ListItemLivre from '@/components/ItemLivre/ListItemLivre.vue';
import DetailItemLivre from '@/components/ItemLivre/DetailItemLivre.vue';
import Vente from '@/components/Admin/Vente.vue';
import HistoriqueTransaction from '@/components/User/HistoriqueTransaction.vue';
import DetailCommande from '@/components/User/DetailCommande.vue';
import SearchResultUser from '@/components/Admin/SearchResultUser.vue';
import SearchResultCommande from '@/components/Admin/SearchResultCommande.vue';
import authService from './auth-service';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkActiveClass: 'is-active',
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignUp
    },
    {
      path: '/listitemlivre',
      name: 'listitemlivre',
      component: ListItemLivre,
    },
    {
      path: '/detailItemLivre',
      name: 'detailItemLivre',
      component: DetailItemLivre,
    },
    {
      path: '/recherche',
      name: 'recherche',
      alias: '/',
      component: Recherche
    },
    {
      path: '/userpage',
      name: 'userpage',
      component: UserPage,
      meta: { requiresAuth: true }
    },
    {
      path: '/historiquetransaction',
      name: 'historiquetransaction',
      component: HistoriqueTransaction,
      meta: { requiresAuth: true }
    },
    {
      path: '/adminpage',
      name: 'adminpage',
      component: AdminPage,
      meta: { requiresAuth: true, role: "Admin" }
    },
    {
      path: '/vente',
      name: 'vente',
      component: Vente,
      meta: { requiresAuth: true, role: "Admin" }
    },
    {
      path: '/searchresultuser',
      name: 'searchresultuser',
      component: SearchResultUser,
      meta: { requiresAuth: true, role: "Admin" }
    },
    {
      path: '/searchresultcommande',
      name: 'searchresultcommande',
      component: SearchResultCommande,
      meta: { requiresAuth: true, role: "Admin" }
    },
    {
      path: '/detailCommande',
      name: 'detailCommande',
      component: DetailCommande,
    },
  ],
});
export default router;

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (authService.tokenIsInvalid()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath },
      });
    }
    else {
      next();
    }
  }
  else {
    next();
  }

  if (to.matched.some((record) => record.meta.role == "Admin")) {
    if (!(localStorage.getItem("role") == "Admin") || authService.tokenIsInvalid()) {
      next({
        path: '/recherche',
      });
    }
    else {
      next();
    }
  }
  else {
    next();
  }
});